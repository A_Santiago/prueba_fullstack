--Creación de la base de datos
create database base;


--Nos movemos a la base de datos
use base;


--Creamos la tabla donde se guardaran los registros
create table DNA(
  id int not null AUTO_INCREMENT primary key,
  mutacion tinyInt,
  secuencia json
);