from flask import Flask
from flask import request
from flask import abort
from flask import render_template
import pymysql.cursors
from flask import jsonify
from businessObject import DNA
from db import Conexion

application = Flask(__name__)

@application.route("/mutation", methods=['POST'])
def mutation():
    dna = request.get_json()
    if DNA.hasMutation(dna["dna"]):
        return "success"
    else:
        abort(403)

@application.route("/stats", methods=['GET'])
def stats():
    conexion = Conexion.crearConexion()
    with conexion.cursor() as cursor:
        sql = "SELECT COUNT(*) FROM DNA WHERE MUTACION = %s"
        cursor.execute(sql, (1,))
        mutaciones = cursor.fetchone()['COUNT(*)']
        cursor.execute(sql,(0,))
        sin_mutacion = cursor.fetchone()['COUNT(*)']
        ratios = mutaciones/sin_mutacion
        
    return jsonify(
        count_mutations = mutaciones,
        count_no_mutation = sin_mutacion,
        ratio = ratios
    )        
    

@application.route("/probarApi", methods=['GET'])
def probarApi():
    return render_template('pruebaApi.html')


if __name__ == '__main__':
    application.debug = True
    application.run()