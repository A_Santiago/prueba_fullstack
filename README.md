# prueba_fullstack
**Ejecucion Local**

Requerimentos:
- Python 8.3.9
- MySQL 8.0
- Sistema Operativo Windows 10



1. Clonar este Repositorio en tu equipo local.
2. Instalar las bibliotecas requeridas de python utilizando los comandos.
    - pip install flask
    - pip install pymysql

3. Consultar el archivo scriptBase.txt en el que se encuentran los comandos para la creación de la base de datos.
    - --Creación de la base de datos
    -     create database base;
    - --Nos movemos a la base de datos
    -     use base;
    - --Creamos la tabla donde se guardaran los registros
    -     create table DNA(
    -     id int not null AUTO_INCREMENT primary key,
    -     mutacion tinyInt,
    -     secuencia json
    -     );

4. Para realizar la conexion utilizar el archivo Conexion.py que se encuentra en la carpeta db y agregar los valores que se requieren. Ejemplo:
    - pymysql.connect(host='**localhost**',
                            user='**root**',
                            password='**root**',
                            database='**base**',
                            cursorclass=pymysql.cursors.DictCursor)
5. Para ejecutar la aplicación ubicamos el archivo application.py y lo ejecutamos desde la linea de comandos:
    -  python application.py
