import pymysql.cursors
from db import Conexion

def hasMutation(dna):
    if horizontal(dna,len(dna)) or vertical(dna,len(dna)) or diagonalInferiorIzquierda(dna, len(dna)) or diagonalInferiorDerecha(dna, len(dna)) or diagonalSuperiorDerecha(dna, len(dna)) or diagonalSuperiorIzquierda(dna, len(dna)):
        guardar(1,dna)
        return True
    else:  
        guardar(0,dna)
        return False
        
def horizontal(dna, tam):
    for f in range(0, tam):
        contador = 1
        aux = dna[f][0]
        for c in range(1, tam):
            if aux == dna[f][c]:
                contador = contador + 1
                if contador == 4:
                    return True
            else:
                aux = dna[f][c]
                contador = 1
    return False

def vertical(dna, tam):
    for c in range(0, tam):
        contador = 1
        aux = dna[0][c]
        for f in range(1, tam):
            if aux == dna[f][c]:
                contador = contador + 1
                if contador == 4:
                    return True
            else:
                aux=dna[f][c]
                contador = 1
    return False

def diagonalInferiorIzquierda(dna, tam):
    incremento = 0
    for y in range(0,tam-1):
        contador = 1
        aux = dna[incremento][0]
        for x in range(incremento, tam-1):
            if aux == dna[x+1][x-incremento+1]:
                contador = contador +1
                if contador == 4:
                    return True
            else:
                aux = dna[x+1][x-incremento+1]
                contador = 1
        incremento = incremento + 1
    return False

def diagonalSuperiorIzquierda(dna, tam):
    incremento = 0
    for y in range(0,tam-1):
        contador = 1
        aux = dna[0][incremento]
        for x in range(incremento, tam-1):
            if aux == dna[x-incremento+1][x+1]:
                contador = contador +1
                if contador == 4:
                    return True
            else:
                aux = dna[x-incremento+1][x+1]
                contador = 1
        incremento = incremento + 1
    return False

def diagonalInferiorDerecha(dna, tam):
    incremento = 0
    for y in range(0,tam):
        contador = 1
        decremento = tam-1
        aux=dna[incremento][decremento]
        for x in range(incremento+1,tam):
            if aux == dna[x][decremento-1]:
                contador = contador+1
                if contador == 4:
                    return True
            else:
                aux = dna[x][decremento-1]
                contador = 1
            decremento = decremento - 1
        incremento = incremento +1
    return False

def diagonalSuperiorDerecha(dna, tam):
    decremento = tam-1
    for y in range(0,tam):
        incremento = 0
        aux = dna[incremento][decremento]
        for x in range(incremento,tam-y-1):
            if aux == dna[x+1][decremento-x-1]:
                contador = contador + 1
                if contador == 4:
                    return True
            else:
                aux = dna[x+1][decremento-x-1]
                contador = 1    
        incremento = incremento + 1
        decremento = decremento - 1
    return False

def guardar(mut, dna):
    conexion = Conexion.crearConexion()
    with conexion:
            with conexion.cursor() as cursor:
                secuencia = secuencia = "\""+str(dna)+"\""
                sql = "INSERT INTO DNA (mutacion, secuencia) VALUES (%s, %s)"
                cursor.execute(sql, (mut,secuencia))
            conexion.commit()